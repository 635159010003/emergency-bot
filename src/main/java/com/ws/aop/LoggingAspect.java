package com.ws.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ws.controller.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class LoggingAspect {

    private static final Logger log = LoggerFactory.getLogger(Controller.class);

    @Autowired
    protected ObjectMapper objectMapper;

    protected String getJson(Object o) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException", e);
        }
        return json;
    }
}
