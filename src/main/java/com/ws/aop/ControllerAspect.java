package com.ws.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class ControllerAspect extends LoggingAspect {

    private static final Logger log = LoggerFactory.getLogger(ControllerAspect.class);

    @Before("execution(* com.ws.controller.*.*(..))")
    public void logBeforeController(JoinPoint joinPoint) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        log.debug("{} {} {} request  : {}", request.getMethod(), request.getRequestURI(), joinPoint.getSignature().getName(), getJson(joinPoint.getArgs()));
    }

    @AfterReturning(value = "execution(* com.ws.controller.*.*(..))", returning = "response")
    public void logAfterController(JoinPoint joinPoint, Object response) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        log.debug("{} {} {} response : {}", request.getMethod(), request.getRequestURI(), joinPoint.getSignature().getName(), getJson(response));
    }
}
