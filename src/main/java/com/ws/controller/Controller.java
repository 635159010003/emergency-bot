package com.ws.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ws.dto.BaseDto;
import com.ws.dto.RegisterDto;
import com.ws.dto.ResponseDto;
import com.ws.model.EmergencyCaseModel;
import com.ws.model.UserModel;
import com.ws.repository.EmergencyCaseRepository;
import com.ws.repository.UserRepository;
import com.ws.service.EmergencyService;

@RestController
@RequestMapping(value = "api/base")
public class Controller {
	
	private static final Logger log = LoggerFactory.getLogger(Controller.class);
	@Autowired
	private EmergencyCaseRepository emergencyCaseRepository;
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EmergencyService emergencyService;
	
	 @GetMapping("/test/{id}")
	 public String getTest(@PathVariable("id") String id) {
	        return id+" : Finish.";
	 }
	 
	 @GetMapping("/test2")
	 public String getTest2() {
//		 EmergencyCaseModel e = new EmergencyCaseModel();
//		 Random ran = new Random();
//		 int x = ran.nextInt(6) + 1000;
//		 e.setId(Long.parseLong(""+x));
//		 e.setUserName("sorn"+x);
//		 e.setProvince("province"+x);
//		 emergencyCaseRepository.save(e);
		 emergencyCaseRepository.deleteById(new Long(1002));;
		 emergencyCaseRepository.deleteById(new Long(1001));;
	       
	        return "success";
	 }
	 
	 @GetMapping("/test3")
	 public ResponseDto getTest3() {
		 ResponseDto r = new ResponseDto();
		 r.setData(emergencyCaseRepository.findAll());
	        return r;
	 }

	 @GetMapping("/test4")
	 public String getTest4() {

		 emergencyCaseRepository.deleteAll();
	       
	        return "success";
	 }
	 @GetMapping("/test5")
	 public String getTest5() {

		 userRepository.deleteAll();
	       
	        return "success";
	 }

	 @GetMapping("/searchEmergency")
	 public ResponseDto searchEmergency(@RequestParam String status,@RequestParam String province
			 ,@RequestParam String district,@RequestParam String subDistrict) throws Exception {
		 	EmergencyCaseModel req = new EmergencyCaseModel();
		 	req.setStatus(status);
		 	req.setProvince(province);
		 	req.setDistrict(district);
		 	req.setSubDistrict(subDistrict);
		 	if(!"TH-10".equals(province)) {
			 	req.setDistrict(null);
			 	req.setSubDistrict(null);
		 	}
			ResponseDto returnData = emergencyService.searchEmergency(req);
			return returnData;
	}	
	 @GetMapping("/searchEmergencyByUsername")
	 public ResponseDto searchEmergencyByUsername(@RequestParam String username) throws Exception {
		 	EmergencyCaseModel req = new EmergencyCaseModel();
		 	req.setUserName(username);
			ResponseDto returnData = emergencyService.searchEmergencyByUsername(req);
			return returnData;
	}	 
	 @GetMapping("/emergency/{id}")
	 public ResponseDto getEmergencyById(@PathVariable("id") Long id) throws Exception {
			
			ResponseDto returnData = emergencyService.getEmergencyById(id);
			return returnData;
	}	 
	 @PostMapping("/sendEmergencyCase")
	    public ResponseDto sendEmergencyCase(@RequestBody EmergencyCaseModel req) throws Exception {
			
		 ResponseDto returnData = emergencyService.sendEmergencyCase(req);
			return returnData;
	    }
	 @PostMapping("/updateEmergencyCase")
	    public ResponseDto updateEmergencyCase(@RequestBody EmergencyCaseModel req) throws Exception {
			
		 ResponseDto returnData = emergencyService.updateEmergencyCase(req);
			return returnData;
	}
	@PostMapping("/register")
    public ResponseDto register(@RequestBody UserModel user) throws Exception {
		log.info("register : username :" + user.getUserName());
		ResponseDto returnData = emergencyService.register(user);
		return returnData;
    }
	@GetMapping("/login")
    public ResponseDto login(@RequestParam String userName,@RequestParam String password) throws Exception {
		ResponseDto returnData =null;
//		RegisterDto register = BaseController.listener.get(userName);
//		if(register.getUserName().equals(userName)&&register.getPassword().equals(password)) {
//			checkLogin="pass";
//		}
		if(!userName.equals("admin")) {
			 returnData = emergencyService.login(userName, password);
		}else {
			 returnData = emergencyService.loginAdmin(userName, password);
		}

		return returnData;
    }
	
	@GetMapping("/findUserModelByUserName")
    public ResponseDto findUserModelByUserName(@RequestParam String userName) throws Exception {
		ResponseDto returnData = emergencyService.findUserModelByUserName(userName);

		return returnData;
    }
	
//	@GetMapping
//    public String viewMethod(@RequestParam String param) throws JsonProcessingException {
//		log.info("GET : " + param);
//		return param;
//    }
//	
//	@PutMapping
//	public ResponseDto updateMethod(@RequestBody BaseDto user) {
//		ObjectMapper objectMapper = new ObjectMapper();
//		try {
//			log.info("PUT : " + objectMapper.writeValueAsString(user));
//		} catch (JsonProcessingException e) {
//			log.error(e.getMessage(), e);
//		}
//		
//		return new ResponseDto(200, "OK");
//	}
//	
//	@PostMapping
//	public ResponseDto saveMethod(@RequestBody BaseDto user) {
//		ObjectMapper objectMapper = new ObjectMapper();
//		try {
//			log.info("POST : " + objectMapper.writeValueAsString(user));
//		} catch (JsonProcessingException e) {
//			log.error(e.getMessage(), e);
//		}
//		
//		return new ResponseDto(200, "OK");
//	}
//	
//	@DeleteMapping
//	public ResponseDto deleteMethod(@RequestParam Long id) {
//		log.info("DELETE : " + id);
//		
//		return new ResponseDto(200, "OK");
//	}
}
