package com.ws.controller;


import com.ws.exceptions.IdAlreadyUsedException;



import com.ws.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("api/example")
public class ExampleController {



//    @Autowired
//    private ExampleService exampleService;

//    @ApiOperation(value = "Search examples", notes = "Search all examples by spec.")
//    @GetMapping
//    public ResponseEntity<Page<ExampleDto>> getAllExampleBySearch(ExampleDto search, Pageable pageable) {
//        Page<ExampleDto> page = exampleService.search(search, pageable).map(ExampleDto::new);
//        return new ResponseEntity<>(page, HttpStatus.OK);
//    }
//
//    @ApiOperation(value = "examples by id", notes = "get examples by id.")
//    @GetMapping("/{id}")
//    public ResponseEntity<ExampleDto> getExampleById(@PathVariable("id") Long id) {
//        return ResponseUtil.wrapOrNotFound(exampleService.findById(id));
//    }
//
//    @ApiOperation(value = "Create examples", notes = "Create example by requestBody.", response = ExampleDto.class)
//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public ResponseEntity createExample(@Valid @RequestBody ExampleDto exampleDto) throws Exception {
//        ExampleModel newExample = exampleService.createExample(exampleDto);
//        return ResponseEntity.created(new URI("/api/example/" + newExample.getId()))
//                .body(newExample);
//    }
//
//    @ApiOperation(value = "Update examples", notes = "Update example by requestBody.", response = ExampleDto.class)
//    @PutMapping
//    public ResponseEntity updateExample(@Valid @RequestBody ExampleDto exampleDto) {
//        return ResponseUtil.wrapOrNotFound(exampleService.updateExample(exampleDto));
//    }
//
//    @ApiOperation(value = "Delete examples", notes = "Delete example by id.")
//    @DeleteMapping("/{id}")
//    public void deleteExample(@PathVariable("id") Long id) {
//        exampleService.deleteById(id);
//    }

    @GetMapping("/test/{id}")
    @Cacheable(value = "test", key = "#id", unless = "#result==null")
    public String getTest(@PathVariable("id") String id) {
        return id;
    }

    @PutMapping("/test/{id}")
    @CachePut(value = "test", key = "#id")
    public String updateTest(@PathVariable("id") String id) {
        return "update : " + id;
    }

    @DeleteMapping("/test/{id}")
    @CacheEvict(value = "test", key = "#id")
    public void deleteTest(@PathVariable("id") String id) {
    }

    @GetMapping("/test/exception")
    public void exceptionTest() throws Exception {
        throw new IdAlreadyUsedException();
    }
}
