package com.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ws.model.EmergencyCaseModel;

public interface EmergencyCaseRepository extends JpaRepository<EmergencyCaseModel, Long> {

}
