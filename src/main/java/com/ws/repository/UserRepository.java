package com.ws.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import com.ws.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {

	Optional<UserModel> findFirstByUserName(String userName);
}
