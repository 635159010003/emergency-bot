package com.ws.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.ws.dto.ResponseDto;
import com.ws.model.EmergencyCaseModel;
import com.ws.model.UserModel;
import com.ws.repository.EmergencyCaseRepository;
import com.ws.repository.UserRepository;

@Service
public class EmergencyService {
	
	@Autowired
	private UserRepository userRepository;
	
	
	@Autowired
	private EmergencyCaseRepository emergencyCaseRepository;
	
	public ResponseDto sendEmergencyCase(EmergencyCaseModel emergencyCaseModel) throws Exception {
		ResponseDto returnData = new ResponseDto();
		emergencyCaseModel.setEmergencyDate(new Date());
		if("--".equals(emergencyCaseModel.getProvinceName())) {
			emergencyCaseModel.setProvinceName(null);
		}
		if("--".equals(emergencyCaseModel.getDistrictName())) {
			emergencyCaseModel.setDistrictName(null);
		}
		if("--".equals(emergencyCaseModel.getSubDistrictName())) {
			emergencyCaseModel.setSubDistrictName(null);
		}
		if("false".equals((emergencyCaseModel.getUserName()+""))) {
			emergencyCaseModel.setUserName(null);
		}
		EmergencyCaseModel emergency = emergencyCaseRepository.save(emergencyCaseModel);
		
		returnData.setData(emergency);
		
		
		return returnData;
	}
	public ResponseDto updateEmergencyCase(EmergencyCaseModel emergencyCaseModel) throws Exception {
		ResponseDto returnData = new ResponseDto();
		
		EmergencyCaseModel db =emergencyCaseRepository.findById(emergencyCaseModel.getId()).get();
		db.setStatus(emergencyCaseModel.getStatus());
		EmergencyCaseModel emergency = emergencyCaseRepository.save(db);
		
		returnData.setData(emergency);
		
		
		return returnData;
	}
	
	public ResponseDto searchEmergency(EmergencyCaseModel emergencyCaseModel) throws Exception {
		ResponseDto returnData = new ResponseDto();
		emergencyCaseModel = setData(emergencyCaseModel);
		Example<EmergencyCaseModel> example = Example.of(emergencyCaseModel);
		List<EmergencyCaseModel> emergencyList = emergencyCaseRepository.findAll(example);
		
		returnData.setData(emergencyList);
		
		
		return returnData;
	}
	public ResponseDto searchEmergencyByUsername(EmergencyCaseModel emergencyCaseModel) throws Exception {
		ResponseDto returnData = new ResponseDto();
		
		Example<EmergencyCaseModel> example = Example.of(emergencyCaseModel);
		List<EmergencyCaseModel> emergencyList = emergencyCaseRepository.findAll(example);
		
		returnData.setData(emergencyList);
		
		
		return returnData;
	}
	public ResponseDto getEmergencyById(Long id) throws Exception {
		ResponseDto returnData = new ResponseDto();
		//https://th.wikipedia.org/wiki/%E0%B9%81%E0%B8%82%E0%B8%A7%E0%B8%87
		
		EmergencyCaseModel emergencyList = emergencyCaseRepository.findById(id).get();
		
		returnData.setData(emergencyList);
		
		
		return returnData;
	}
	public ResponseDto register(UserModel userModel) throws Exception {
		ResponseDto returnData = new ResponseDto();
		
		Optional<UserModel> checkUserName = userRepository.findFirstByUserName(userModel.getUserName());
		if(checkUserName.isPresent()) {
			returnData.setMessage("UserName Already Exist");
		}else if(userModel.getUserName().equals("admin")) {
			returnData.setMessage("UserName Already Exist");
		} {
			UserModel u = userRepository.save(userModel);
			returnData.setMessage("Success");
			returnData.setData(u);
		}
		
		
		return returnData;
	}
	
	public ResponseDto findUserModelByUserName(String userName) throws Exception {
		ResponseDto returnData = new ResponseDto();
		
		UserModel u = userRepository.findFirstByUserName(userName).get();
		
		returnData.setData(u);
		
		
		return returnData;
	}
	
	public ResponseDto login(String userName, String password) throws Exception {
		ResponseDto returnData = new ResponseDto();
		
		Optional<UserModel> checkUserName = userRepository.findFirstByUserName(userName);
		
		if(checkUserName.isPresent()) {
			UserModel register = checkUserName.get();
			
			if(userName.equals(register.getUserName())&&password.equals(register.getPassword())) {
				returnData.setMessage("Success");
			}else {
				returnData.setMessage("Password Incorrect");
			}
			
			
		}else {
			returnData.setMessage("UserName Incorrect");
		}
		
		return returnData;
	}
	public ResponseDto loginAdmin(String userName, String password) throws Exception {
		ResponseDto returnData = new ResponseDto();
		
		
			
			if(userName.equals("admin")&&password.equals("admin")) {
				returnData.setMessage("AdminLoginSuccess");
			}else {
				returnData.setMessage("Password Incorrect");
			}
			
		
		
		return returnData;
	}
	public EmergencyCaseModel setData(EmergencyCaseModel emergencyCaseModel)  {
		if("".equals(emergencyCaseModel.getStatus())) {
			emergencyCaseModel.setStatus(null);
		}
		if("".equals(emergencyCaseModel.getProvince())) {
			emergencyCaseModel.setProvince(null);
		}
		if("".equals(emergencyCaseModel.getDistrict())) {
			emergencyCaseModel.setDistrict(null);
		}
		if("".equals(emergencyCaseModel.getSubDistrict())) {
			emergencyCaseModel.setSubDistrict(null);
		}
		return emergencyCaseModel;
	}

}
