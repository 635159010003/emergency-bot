package com.ws.dto;

import java.io.Serializable;

public class ResponseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2372223224981339019L;

	private int code;
	
	private String message;
	
	private String reason;
	
	private Object data;
	
	public ResponseDto() {}
	
	public ResponseDto(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ResponseDto(int code, String message, String reason) {
		super();
		this.code = code;
		this.message = message;
		this.reason = reason;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
}
