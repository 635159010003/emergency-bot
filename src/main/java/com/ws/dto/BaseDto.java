package com.ws.dto;

import java.io.Serializable;

public class BaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3933478792532609949L;

	private String attr1;
	
	private Integer attr2;
	
	private Long attr3;

	public String getAttr1() {
		return attr1;
	}

	public void setAttr1(String attr1) {
		this.attr1 = attr1;
	}

	public Integer getAttr2() {
		return attr2;
	}

	public void setAttr2(Integer attr2) {
		this.attr2 = attr2;
	}

	public Long getAttr3() {
		return attr3;
	}

	public void setAttr3(Long attr3) {
		this.attr3 = attr3;
	}
	
}
