package com.ws.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "EMERGENCY_CASE")
public class EmergencyCaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8645954585185956629L;
	
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMERGENCY_CASE_SEQ")
    @SequenceGenerator(sequenceName = "emergency_case_seq", allocationSize = 1, name = "EMERGENCY_CASE_SEQ")
    private Long id;
	
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;
	
    @Column(name = "NAME")
    private String name;

    @Column(name = "ADDRESS")
    private String address;
    
    @Column(name = "PROVINCE")
    private String province;
    
    @Column(name = "PROVINCE_NAME")
    private String provinceName;
    
    
    @Column(name = "DISTRICT")
    private String district;
    
    @Column(name = "DISTRICT_NAME")
    private String districtName;
    
    
    @Column(name = "SUBDISTRICT")
    private String subDistrict;
    
    @Column(name = "SUBDISTRICT_NAME")
    private String subDistrictName;
    
    @Column(name = "ADDRESS_DETAIL")
    private String addressDetail;
    
    @Column(name = "DETAIL")
    private String detail;
    
    @Column(name = "LATITUDE_LONGTITUDE")
    private String latitude_longtitude;
    
    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "EMERGENCY_DATE")
	private Date emergencyDate;

    
    
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEmergencyDate() {
		return emergencyDate;
	}

	public void setEmergencyDate(Date emergencyDate) {
		this.emergencyDate = emergencyDate;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public String getSubDistrictName() {
		return subDistrictName;
	}

	public void setSubDistrictName(String subDistrictName) {
		this.subDistrictName = subDistrictName;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getLatitude_longtitude() {
		return latitude_longtitude;
	}

	public void setLatitude_longtitude(String latitude_longtitude) {
		this.latitude_longtitude = latitude_longtitude;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}
    
    
    
}
