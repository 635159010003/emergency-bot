package com.ws.constants;

public interface BaseConstants {
    String SPRING_PROFILE_DEVELOPMENT = "dev";
    String SPRING_PROFILE_PRODUCTION = "prod";
    String SPRING_PROFILE_NO_LIQUIBASE = "no-liquibase";
    String SPRING_PROFILE_SWAGGER = "swagger";
}
