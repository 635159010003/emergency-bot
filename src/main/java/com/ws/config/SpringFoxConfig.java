package com.ws.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SpringFoxConfig {
    @Bean
    public Docket api() {
    	 ParameterBuilder parameterBuilder = new ParameterBuilder();
    	    parameterBuilder.name("Authorization")
    			    .modelRef(new ModelRef("string"))
    			    .parameterType("header")
    			    .description("JWT token")
    			    .required(true)
    			    .defaultValue("Bearer ").build();
    	    List<Parameter> parameters = new ArrayList<>();
    	    
    	    parameters.add(parameterBuilder.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(paths())
                .build().globalOperationParameters(parameters);
    }

    private Predicate<String> paths() {
        return regex("/.*");
    }
}


