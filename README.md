# Base Service

Base service [Spring Boot](http://projects.spring.io/spring-boot/) for CGD-Pension.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.ws.BaseServiceApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## h2 database
```shell
http://[domain]:[port]/base-service/h2-console
```
## Swagger
```shell
http://[domain]:[port]/base-service/swagger-ui.html
```

## Common Lib
```
run maven command :  mvn install:install-file -Dfile=common-lib-0.0.1-SNAPSHOT.jar -DgroupId=com.ws.pension -DartifactId=common-lib -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar
to install common-lib that included in libs folder, you need to access to its folder before running the command.
```
